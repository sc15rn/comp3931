# Required imports
from django.shortcuts import HttpResponseRedirect, redirect

# Middware - purpose: Redirect users to login page if they don't have access to the page they are trying to access.
class AuthRequiredMiddleware(object):
    def __init__(self, get_response):

        self.get_response = get_response

    def __call__(self, request):
        
        # Redirect Logic
        if not request.user.is_authenticated and request.path != ('/login/'):
            print("User redirected from " + request.path + " to /login/")
            return redirect("/login/")

        response = self.get_response(request)

        return response