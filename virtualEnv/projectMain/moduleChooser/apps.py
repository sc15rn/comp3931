from django.apps import AppConfig

class ModulechooserConfig(AppConfig):
    name = 'moduleChooser'
