# All imports
from django.shortcuts import render, redirect, get_object_or_404	# shortcut imports
from django.http import HttpResponse, Http404						# http response and 404
from django.contrib.auth import authenticate, login, logout 		# auth imports
from django.contrib.auth.models import User							# user model import
from django.views import generic									# generic view
from django.views.generic import View								# view import
from moduleChooser.forms import UserRegisterForm, UserLoginForm		# form imports
from moduleChooser.models import Student, Course, Module			# Model imports
import datetime							

# DASHBOARD - Page Logic
def dashboard(request):

	# check if the user's current level modules have been chosen
	SubmittedBool = False

	# Filtered search [Year: students current level]
	yearSubmittedCheck = request.user.student.selected_modules.filter(level=request.user.student.year_of_study)

	# Check to if the object is true if so set the bool to true
	if yearSubmittedCheck:
		SubmittedBool = True

	# Content to pass to template
	context = {
		'title': 'Dashboard',
		'all_users': User.objects.all(),
		'all_courses': Course.objects.all(),
		'all_modules': Module.objects.all(),
		'user': request.user,
		'SubmittedBool': SubmittedBool,
	}

	# Return the request and the template with the context
	return render(request, 'moduleChooser/dashboard.html', context)

# REGISTRATION - Page Logic
class UserRegisterFormView(View):

	# Get the UserRegisterForm
	form_class = UserRegisterForm
	# Store the registration template name
	template_name = 'moduleChooser/registration_form.html'
	# Set form class to None
	form = form_class(None)

	# Content to pass to template
	context = {
		'title': 'Register',
		'form': form
	}

	# Display blank form - GET method
	def get(self, request):
		form = self.form_class(None)
		return render(request, self.template_name, self.context)

	# Process the form data submitted - POST method
	def post(self, request):

		# Post method
		form = self.form_class(request.POST)

		# Check if the submitted form is valid
		if form.is_valid():

			# Creates obj from form, doesn't save to db yet
			user = form.save(commit=False)		

			# Cleaned (normalised) data
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			# Calls method to set the password
			user.set_password(password)
			
			# Save the user object
			user.save()

			# Returns user object if credentials corretctt
			user = authenticate(username=username, password=password)

			# Check user exists
			if user is not None:
				# Check user is active account
				if user.is_active:
					login(request, user)
					return redirect('dashboard')

		# Return the request and the template with the context
		return render(request, self.template_name, self.context)


# LOGIN - Page Logic
class UserLoginFormView(View):

	# Get the UserLoginForm
	form_class = UserLoginForm
	# Store the login template name
	template_name = 'moduleChooser/login_form.html'
	# Set form class to None
	form = form_class(None)

	# Content to pass to template
	context = {
		'title': 'Login',
		'form': form
	}
	
	# Display blank form - GET method
	def get(self, request):
		form = self.form_class(None)
		return render(request, self.template_name, self.context)

	#process the form data - POST method
	def post(self, request):

		# Post method
		form = self.form_class(request.POST)

		# Check if the submitted form is valid
		if form.is_valid():

			#cleaned (normalised) data
			username = form.cleaned_data['username']
			password = form.cleaned_data['password']

			# returns user object if credentials correct
			user = authenticate(username=username, password=password)

			# Check user exists
			if user is not None:
				# Check user is active account
				if user.is_active:
					login(request, user) 
					return redirect('dashboard')

		# Return the request and the template with the context
		return render(request, self.template_name, self.context)


# LOGOUT - Page Logic
def logoutView(request):
	# The session data for the current request is completely cleaned out.
	logout(request)
	# User returned to login page
	return redirect('login')


# MODULE SELECTION - Page Logic
def year(request, year_id):

	# Filtered search[Module type: compulsory, Year: Given year]
	compulsoryModules = request.user.student.course.modules.filter(level=year_id, module_type=1)
	# Store in session data
	request.session['compulsoryModules'] = list(compulsoryModules.values())

	# Filtered search[Module type: Optional, Year: Given year]
	optionalModules = request.user.student.course.modules.filter(level=year_id, module_type=2)
	# Store in session data
	request.session['optionalModules'] = list(optionalModules.values())

	# Store year
	request.session['year'] = year_id

	# Need to calculate the credits available to spend on optionals
	# Add up compulsary credits, take away from 120
	coreCreditCount = 0
	for module in compulsoryModules:
		coreCreditCount += module.credits

	availableCredits = 120 - coreCreditCount
	
	# Grab all modules from a users Selected Modules table
	selectedModules = request.user.student.selected_modules.all()

	# Intialise Array ready for new list of modules with the new .msg & .futuremodules attribute 
	updatedFilteredModules = []

	# For each filtered module check if it has a matching selected module, if so append msg field to new list
	for module in optionalModules:
		# Initialise a list on module to store the future modules from a given module
		module.futuremodules = [] 

		# Loops through all modules in users course
		for curmodule in request.user.student.course.modules.all():
			# See if any of the modules dependancies match the filter module we are iterating
			mod = curmodule.dependancies.all().filter(module_code=module.module_code).first()
			# if found append to futuremodules
			if mod:
				# Print(curmodule.module_title + " has a dependancy: " + curmodule.module_title + " for " + module.module_title)
				module.futuremodules.append(curmodule)

		# Check to see if the modules dependancies are in [selected_module] - Determines if we meet the dependancies or not
		result = all( item in list(request.user.student.selected_modules.all()) for item in list(module.dependancies.all()) )
		if result:
			# If true then give the module the message "Satisfied"
			module.msg = "Satisfied."
			module.checkboxClass = "checkbox"
			updatedFilteredModules.append(module) 
		else:
			# If false then give the module the message "Not Satisfied"
			module.msg = "Not Satisfied."
			module.checkboxClass = "readonly"
			updatedFilteredModules.append(module) 

	# Objects to send through to our template
	context = {
		'title': 'Breakdown',
		'user': request.user,
		'year_id': year_id,
		'Modules': updatedFilteredModules,
		'SelectedModules': selectedModules,
		'availableCredits': availableCredits,
	}

	# POST recieved, handle post data
	if request.method == "POST":
		# Post method logic here
		# Retrieves the array list from the post method, contains array of module codes
		selected = request.POST.getlist('module')

		# Retrieves the selected modules from the session
		request.session['selectedModules'] = request.POST.getlist('module')

		# Redirect code to next page
		return redirect('confirmationOne')

	# Return the request and the template with the context
	return render(request, 'moduleChooser/year.html', context)


# MODULE CONFIRMATION SCREEN 1 - Page Logic
def confirmationOne(request):

	# Get the year from the session
	year_id = request.session.get('year')

	# FUTURE DEPENDANT MODULES #
	# Get the selectedModules from the session
	selectedModules = request.session.get('selectedModules')
	FutureDependantModules = []

	# Loop through the selected modules
	for module in selectedModules:
		retrievedMod = request.user.student.course.modules.get(module_code=module)
		retrievedMod.futuremodules = []

		# Loops through all modules in users course except the current year they are selecting modules for
		for curmodule in request.user.student.course.modules.all().exclude(level=year_id):
			# See if any of the modules dependancies match the filter module we are iterating
			mod = curmodule.dependancies.all().filter(module_code=retrievedMod.module_code).first()
			# If found append to futuremodules
			if mod:
				# Print(curmodule.module_title + " has a dependancy: " + curmodule.module_title + " for " + module.module_title)
				retrievedMod.futuremodules.append(curmodule)
		# Append the retrieved mod
		FutureDependantModules.append(retrievedMod)
		
	# FUTURE MODULES NOW UNAVAILABLE #
	# Produce a list of module objects that the user didn't select only from the year they are choosing for
	notSelectedmodules = request.user.student.course.modules.exclude(module_code__in=[m.module_code for m in FutureDependantModules]).filter(level = year_id)   
	FutureModulesNowUnavailable = []

	# Loops through modules in the modules not selected by the user
	for module in notSelectedmodules:

		retrievedMod = notSelectedmodules.filter(module_code=module.module_code, module_type=2).first()
		if retrievedMod:
			# Creates a new futermodules field to be populated
			retrievedMod.futuremodules = []
			
			#Loops through all modules in users course
			for curmodule in request.user.student.course.modules.all():
				#see if any of the modules dependancies match the filter module we are iterating - exclude(level__in=notCurrentYearList)
				mod = curmodule.dependancies.all().filter(module_code=retrievedMod.module_code).first()
				# if found append to futuremodules
				if mod:
					#print(curmodule.module_title + " has a dependancy: " + curmodule.module_title + " for " + module.module_title)
					retrievedMod.futuremodules.append(curmodule)
			# Append the retrieved mod
			FutureModulesNowUnavailable.append(retrievedMod)
	
	# Objects to send through to the template
	context = {
		'title': 'Selection Summary',
		'user': request.user,
		'year_id': year_id,
		'selected': request.session.get('selectedModules'),
		'compulsoryModules': request.session.get('compulsoryModules'),
		'optionalModules': request.session.get('optionalModules'),
		'FutureDependantModules': FutureDependantModules,
		'FutureModulesNowUnavailable': FutureModulesNowUnavailable,
	}

	# Return the request and the template with the context
	return render(request, 'moduleChooser/confirmationOne.html', context)


def confirmationTwo(request):

	#Get the year from the session
	year_id = request.session.get('year')

	# Filtered search[Module type: compulsory, Year: Given year]
	compulsoryModules = request.user.student.course.modules.filter(level=year_id, module_type=1)

	# Get selected modules from the session
	SessionSelectedModules = request.session.get('selectedModules')
	SelectedModules = []

	# Loop through the selected modules
	for module in SessionSelectedModules:
		retrievedMod = request.user.student.course.modules.get(module_code=module)
		SelectedModules.append(retrievedMod)

	# POST logic to posting to the database
	if request.method == "POST":
		# Post method logic here

		# Append the selected optional modules to the users selected modules table in DB
		for module in SelectedModules:
			# Gets the object of the module
			# RetrievedMod = request.user.student.course.modules.get(module_code=module)
			# Stores the object into the selected users table
			request.user.student.selected_modules.add(module)

		# Append the compulsary modules to the users selected modules
		for module in compulsoryModules:
			request.user.student.selected_modules.add(module)

		#Put redirect code here to next page
		return redirect('dashboard')

	
	# Objects to send through to our template
	context = {
		'title': 'Summary',
		'user': request.user,
		'selected': SelectedModules,
		'compulsoryModules': request.session.get('compulsoryModules'),
		'optionalModules': request.session.get('optionalModules'),
	}
	
	# Return the request and the template with the context
	return render(request, 'moduleChooser/confirmationTwo.html', context)