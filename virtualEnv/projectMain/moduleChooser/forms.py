# Required imports
from django.contrib.auth.models import User
from moduleChooser.models import Student, Course, Module	#DB imports
from django import forms

# Defines the registration class
class UserRegisterForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    class Meta:     #info about Class
        model = User
        fields = ['username', 'email', 'password']

# defines the login class
class UserLoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput)
    password = forms.CharField(widget=forms.PasswordInput)
