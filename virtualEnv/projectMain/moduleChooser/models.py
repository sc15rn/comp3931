# Required imports
from django.db import models
from django.contrib.auth.models import User                                     #imports the Django Auth table

#Detail for each module
class Module(models.Model):
    module_title = models.CharField(max_length=100)                             # Module title
    module_code = models.CharField(max_length=30)                               # Module Code
    module_type = models.IntegerField(default=1)                                # 1=Core 2=Optional
    level = models.IntegerField(default=1)
    credits = models.IntegerField(default=10)                                   # Module Credits
    semester = models.IntegerField(default=1)                                   # 1,2,3 3=both semesters
    module_page = models.CharField(max_length=300, default='#')                 # Stores modules page url
    dependancies = models.ManyToManyField("Module", blank=True)                 # can be blank as may not have dependancies
    def __str__(self):                                                          # String representation of the object, seen in shell - User.objects.all()
        return self.module_code + " - " +  self.module_title                    # Provides summary of tables data

#Detail for each course
class Course(models.Model):
    course_title = models.CharField(max_length=100)                             # Title of the course
    course_length = models.IntegerField()                                       # Length in years
    modules = models.ManyToManyField(Module)                                    # modules in the Course
    def __str__(self):                                                          # String representation of the object, seen in shell - User.objects.all()
        return self.course_title

# Details for the student
class Student(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)                 #One to One relationship to the django auth table
    year_of_study = models.IntegerField(default=1)                              #Interger stores year of study
    course = models.ForeignKey('Course', on_delete=models.CASCADE)              #Foreign key to course table
    selected_modules = models.ManyToManyField(Module, blank=True)               #Selected Modules many to many relationship to course
    def __str__(self):                                                          #String representation of the object, seen in shell - User.objects.all()
        return self.user.username
