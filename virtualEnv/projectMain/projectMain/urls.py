"""projectMain URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path


#Imports for views objects
from moduleChooser.views import login, logoutView, dashboard, UserRegisterFormView, UserLoginFormView, year, confirmationOne, confirmationTwo

urlpatterns = [
    path('admin/', admin.site.urls),        #path for admin - Django default
    path('login/', UserLoginFormView.as_view(), name='login'),                  #path for login page
    path('logout/', logoutView, name='logoutView'),          #path for logout page
    path('dashboard/', dashboard, name='dashboard'),          #path for dash page
    path('register/', UserRegisterFormView.as_view(), name='register'),        #path for register form
    path('year/<int:year_id>', year, name='year'),
    path('confirmationOne/', confirmationOne, name='confirmationOne'),
    path('confirmationTwo/', confirmationTwo, name='confirmationTwo'),
]
